<?php

namespace Xngage\Bundle\RecaptchaBundle\Form\Extension;

use Xngage\Bundle\GoogleRecaptchaBundle\Form\Type\GoogleRecaptchaType;
use Xngage\Bundle\GoogleRecaptchaBundle\Validator\Constraints\IsTrue as RecaptchaTrue;
use Xngage\Bundle\RecaptchaBundle\DependencyInjection\Configuration;
use Oro\Bundle\ConfigBundle\Config\ConfigManager;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormBuilderInterface;

abstract class AbstractRecaptchaTypeExtension extends AbstractTypeExtension
{
    const DEFAULT_THEME = 'light';
    const DEFAULT_SIZE = 'normal';
    const PRIVATE_KEY = 'private_key';
    const PUBLIC_KEY = 'private_key';

    /** @var ConfigManager */
    protected $configManager;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($this->isProtected()) {

            $builder->add('recaptcha', GoogleRecaptchaType::class, [
                'attr' => [
                    'options' => [
                        'theme' => $this->getTheme(),
                        'type'  => 'image',
                        'size'  => $this->getSize(),
                    ]
                ],
                'mapped'      => false,
                'constraints' => [
                    new RecaptchaTrue(),
                ],
            ]);
        }
    }

    /**
     * @param ConfigManager $configManager
     */
    public function setConfigManager(ConfigManager $configManager)
    {
        $this->configManager = $configManager;
    }

    /**
     * @return mixed
     */
    protected function getTheme()
    {
        return $this->getConfiguration(Configuration::THEME, self::DEFAULT_THEME);
    }

    /**
     * @return mixed
     */
    protected function getSize()
    {
        return $this->getConfiguration(Configuration::SIZE, self::DEFAULT_SIZE);
    }

    /**
     * @return mixed
     */
    protected function getPrivateKey()
    {
        return $this->getConfiguration(Configuration::PRIVATE_KEY, self::PRIVATE_KEY);
    }

    /**
     * @return mixed
     */
    protected function getPublicKey()
    {
        return $this->getConfiguration(Configuration::PUBLIC_KEY, self::PUBLIC_KEY);
    }

    /**
     * Get configuration option
     * @param string $key
     * @param $default
     * @return mixed
     */
    protected function getConfiguration(string $key, $default = null)
    {
        return $this->configManager->get(Configuration::getConfigKeyByName($key)) ?? $default;
    }

    /**
     * @return boolean
     */
    abstract public function isProtected();

}