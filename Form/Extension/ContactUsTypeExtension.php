<?php

namespace Xngage\Bundle\RecaptchaBundle\Form\Extension;

use Xngage\Bundle\RecaptchaBundle\DependencyInjection\Configuration;
use Oro\Bundle\ContactUsBundle\Form\Type\ContactRequestType;

class ContactUsTypeExtension extends AbstractRecaptchaTypeExtension
{
    public function getExtendedType()
    {
        return ContactRequestType::class;
    }

    /**
     * Protect the Contact Us Form?
     * @return boolean
     */
    public function isProtected()
    {
        return $this->getConfiguration(Configuration::PROTECT_CONTACT_FORM, false);
    }

    /**
     * {@inheritdoc}
     */
    public static function getExtendedTypes(): iterable
    {
        return [ContactRequestType::class];
    }
}
