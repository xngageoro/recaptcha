<?php

namespace Xngage\Bundle\RecaptchaBundle\Form\Extension;

use Xngage\Bundle\RecaptchaBundle\DependencyInjection\Configuration;
use Oro\Bundle\CustomerBundle\Form\Type\FrontendCustomerUserRegistrationType;

class RegistrationTypeExtension extends AbstractRecaptchaTypeExtension
{
    public function getExtendedType()
    {
        return FrontendCustomerUserRegistrationType::class;
    }

    /**
     * Protect the Registration Form?
     * @return boolean
     */
    public function isProtected()
    {
        return $this->getConfiguration(Configuration::PROTECT_REGISTRATION, false);
    }

    /**
     * {@inheritdoc}
     */
    public static function getExtendedTypes(): iterable
    {
        return [FrontendCustomerUserRegistrationType::class];
    }

}
